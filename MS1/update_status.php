<?php
session_start();

$userid = $_SESSION['id'];
$reqID = $_POST['request'];
$status = $_POST['status'];

include('database.php');

	try{
        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                                   // set the PDO error mode to excepti
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $sql = "UPDATE requests SET r_status= :status, r_contributor= :userid WHERE r_id = :rid";

		$stm = $conn->prepare($sql);
		
		$stm->bindParam(':rid', $reqID);
		$stm->bindParam(':status', $status);
		if($status == "REQUESTED"){
			$null = null;
			$stm->bindParam(':userid', $null);
		}
		else
		{
			$stm->bindParam(':userid', $userid);
		}


		$stm->execute();

		
	}
	 catch(PDOException $e)
     {
         echo $sql . "<br/>" . $e->getMessage();
     }
       

?>