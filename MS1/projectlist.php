<?php include('topbit.inc'); ?>
<title>Project | Project List</title>
<?php include('middlebit.inc'); ?>

<?php if(!isset($_SESSION['loggedin'])){ 

	header("Location: index.php"); /* Redirect browser */

	exit();

	}
?>

<?php
	include('database.php');
	$user_id = $_SESSION['id'];
	// find projects in projects table
	$listsql = "SELECT * FROM projects";
	$listcount = ($listresult = mysqli_query($con, $listsql))?mysqli_num_rows($listresult):0;
	// find user's selected projects
	$projectsql = "SELECT pu.p_id, pu.u_id, p.p_title 
	FROM projectsusers as pu 
	INNER JOIN projects as p ON pu.p_id = p.p_id WHERE pu.u_id = '$user_id'";
	
	$projectcount = ($projectresult = mysqli_query($con, $projectsql))?mysqli_num_rows($projectresult):0;
	
	if($listcount > 0){
		$listdata = $projectdata = array();
		while($listrow = mysqli_fetch_assoc($listresult)){
			$listdata[] = $listrow;
		}
		while($projectrow = mysqli_fetch_assoc($projectresult)){
			$projectdata[] = $projectrow;
		}
		?>
		<h2>Please select project(s) from the list below</h2>
		<br/>
		<form method="post" action="index.php">
			<input type="hidden" name="action" value="selectAction">
			<table style="width:100%">
				<tr>
					<th>Project ID</th>
					<th>Project Title</th>
					<th>Select Project</th>
				</tr> 
				<?php 
				foreach ($listdata as $listrow){
					if($projectcount > 0){
						$selected = false;
						foreach ($projectdata as $projectrow){
							if($listrow["p_id"] == $projectrow["p_id"]){
								$selected = true;
							}
						}
						if($selected == false){?>
							<tr>
								<td><?php echo $listrow["p_id"];?></td>
								<td> 
									<?php echo $listrow["p_title"];?>
								</td>
								<td>
									<input type="checkbox" name="project[]" value="<?php echo $listrow["p_id"];?>"/>
								</td>
							</tr>
						<?php }
					}
					else{?>
						<tr>
							<td><?php echo $listrow["p_id"];?></td>
							<td><?php echo $listrow["p_title"];?> </td>
							<td><input type="checkbox" name="project[]" value="<?php echo $listrow["p_id"];?>"/></td>
						</tr>
					<?php 
				    }
				}?>
			</table>
			<br/><br/>
			<input type="submit" value="Select Project(s)"/>
			<input type="button" value="Cancel" OnClick="window.location.href='index.php'">  </input>
		</form>
		<?php
	}
	else{
		?>
		<h2>There are no available projects</h2>
		<?php
	}
?>
<?php include('endbit.inc'); ?>