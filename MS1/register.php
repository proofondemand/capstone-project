<?php include('topbit.inc'); ?>
<title>Project | Register</title>
<?php include('middlebit.inc'); ?>
<p><h1 class="contentheading">Register</h1></p>
<?php
	include('database.php');
	
	$fnameError = $lnameError = $emailError = $pwError = "";
	$firstname = $lastname = $email = $password = "";
	$registered = false;
	// php handling of register requests
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		if (empty($_POST["firstname"])) {
			$fnameError = "First name is required";
		}
		else {
			$firstname = trim($_POST['firstname']);
			if (!preg_match("/^[a-zA-Z ]*$/",$firstname)) {
				$fnameError = "Only letters and whitespace allowed"; 
			}
		}
		if (empty($_POST["lastname"])) {
			$lnameError = "Last name is required";
		}
		else {
			$lastname = trim($_POST['lastname']);
			if (!preg_match("/^[a-zA-Z ]*$/",$lastname)) {
				$lnameError = "Only letters and whitespace allowed"; 
			}
		}
		if (empty($_POST["email"])) {
			$emailError = "Email is required";
		}
		else {
			$email = trim($_POST["email"]);
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				$emailError = "Not a valid email address"; 
			}
		}
		if (empty($_POST["password"])) {
			$pwError = "Password is required";
		}
		else {
			$password = trim($_POST["password"]);
			if (strlen($password)<4 || strlen($password)>32) {
				$pwError = "Password must be between 4 and 32 characters";
			}
		}
		$email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
		// encrypt password with the sha1 method
		$password = sha1(trim($_POST['password']));
		
		$sql = "SELECT * FROM users";
		$count = ($result = mysqli_query($con, $sql))?mysqli_num_rows($result):0;
		// check if email address is already in use
		if($count > 0){
			while($row = mysqli_fetch_assoc($result)){
				if($email == $row['u_email']){
					$emailError = "Email is already in use";
					break;
				}
			}
		}

	    ini_set('display_errors',1);
	    
	    $selected = "SELECT * FROM users;";
		mysqli_query($con, $selected) or die ('Error selecting database: '.mysql_error());


		// if no errors occur, register details in users table
		if ($fnameError == "" && $lnameError == "" && $emailError == "" && $pwError == "") {
			$regsql = "INSERT INTO users (u_id, u_email, u_pw, u_fname, u_lname) VALUES (NULL, '$email', '$password', '$firstname', '$lastname');";
			if(mysqli_query($con, $regsql)){
							$registered = true;
				echo "Registration successful. <br /> You will be redirected to the login page. <br />";
				header( "refresh:5;url=login.php" );
				echo "<a href=\"login.php\">Click here if you are not redirected in 5 seconds.</a><br />";
			}else{
				    echo "Number of rows affected: " . mysql_affected_rows() . "<br>";
			}


			/*
			if(mysqli_query($con, $regsql)){
				$registered = true;
				echo "Registration successful. <br /> You will be redirected to the login page. <br />";
				header( "refresh:5;url=login.php" );
				echo "<a href=\"login.php\">Click here if you are not redirected in 5 seconds.</a><br />";
		    }
		    else{
		    	//$registered = true;
		    	error_reporting(E_ALL);
		    	echo "Registration failed.".mysql_errno();
		    }*/
		}
	}
	// if registration fails, insert previous details into form
	if(!isset($_SESSION['loggedin']) && $registered == false){
		if(isset($_POST['firstname'])){
			$firstname = trim($_POST['firstname']);
		}
		if(isset($_POST['lastname'])){
			$lastname = trim($_POST['lastname']);
		}
		if(isset($_POST['email'])){
			$email = trim($_POST['email']);
		}
	?>
		<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
		<br/>First Name:<br />
		<input type="text" name="firstname" size="25" value="<?php echo $firstname ?>" />
		<span class="error"><?php echo $fnameError;?></span>
		<br /><br />
		Last Name:<br />
		<input type="text" name="lastname" size="25" value="<?php echo $lastname ?>"/>
		<span class="error"><?php echo $lnameError;?></span>
		<br /><br />
		Email:<br />
		<input type="text" name="email" size="50" value="<?php echo $email ?>"/>
		<span class="error"><?php echo $emailError;?></span>
		<br /><br />
		Password:<br />
		<input type="password" value="" name="password" />
		<span class="error"><?php echo $pwError;?></span>
		<br /><br />
		<input type="submit" value="Register" /><br /><br /></form>
	<?php
	}
	// do not show registration form if already logged in
	else if(isset($_SESSION['loggedin'])){
		echo "<p>You are already logged in as a registered user!</p>";
	}
?>
<?php include('endbit.inc'); ?>