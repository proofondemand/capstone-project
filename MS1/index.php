<?php include('topbit.inc'); ?>
<?php 
	if(!isset($_SESSION['loggedin'])){
		echo "<title>Project | Login</title>";
	}
	else{
		echo "<title>Project | Member Page</title>";
	}
?>
<?php include('middlebit.inc'); ?>
<?php
	include('database.php');
	
	$loginError = "";
	// php handling of requests
	if($_SERVER["REQUEST_METHOD"] == "POST") {
		// php handling of login requests
		if($_POST['action'] == "Login"){
			$email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
			$password = sha1(trim($_POST['password']));
			// find in database the email and password entered by user that is logging in
			$sql = "SELECT * FROM users WHERE u_email='$email' and u_pw='$password'";
			$count = ($result = mysqli_query($con, $sql))?mysqli_num_rows($result):0;
			// if found matching email and password, set user information as session variables
			if($count == 1){
				$_SESSION['loggedin'] = true;
				while($row = mysqli_fetch_assoc($result)){
					$_SESSION['id'] = $row["u_id"];
					$_SESSION['email'] = $row["u_email"];
					$_SESSION['firstname'] = $row["u_fname"];
					$_SESSION['lastname'] = $row["u_lname"];
				}
				echo "<p>Login successful!</p>";
				echo "<p>Welcome, ".$_SESSION['firstname']." ".$_SESSION['lastname']."!</p>";
				echo "You will be redirected to the index page. <br />";
				echo "<a href=\"index.php\">Click here if you are not redirected in 5 seconds.</a><br />";
				$secondsWait = 1; 
				echo '<meta http-equiv="refresh" content="'.$secondsWait.'">';
				
			}
			// if matching email and password not found, show relevant error message
			else{
				$loginError = "User does not exist or password is incorrect";
			}
		}
		// php handling of logout requests
		else if($_POST['action'] == "Log Out"){
			// get rid of all session variables
			session_destroy();
			echo "<p>You have been logged out successfully!</p>";
			header("Refresh: 0;");
			echo "You will be redirected to the index page. <br />";
			echo "<a href=\"index.php\">Click here if you are not redirected in 5 seconds.</a><br />";
			$secondsWait = 1; 
			echo '<meta http-equiv="refresh" content="'.$secondsWait.'">';
			
		}
		else{
			header("Location: login.php");
		}
	}
	if(!isset($_SESSION['loggedin'])){
	?>	<p><h1 class="contentheading">Log In</h1></p>
		<span class="error"><?php echo $loginError;?></span>
		<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
			<p>Email:<br/>
			<input type="text" name="email" size="20"/><br/></p>
			<p>Password:<br/>
			<input type="password" name="password"/><br/></p>
			<p><input type="submit" name="action" value="Login"/><br/></p>
		</form>
	<?php
	}
	// show page as member page instead if already logged in
	else if(isset($_SESSION['loggedin']) && $_SERVER["REQUEST_METHOD"] != "POST"){
	?>	<p><h1 class="contentheading">Member Page</h1></p>
		<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
			<p><em>Full Name:</em> <?php echo trim($_SESSION['firstname'])." ".trim($_SESSION['lastname']);?><br/>
			<a href="subscriptions.php?type=request"><em>Number of Requests:</em> <?php
				$userid = $_SESSION['id'];
				$reqsql = "SELECT r_id FROM requests WHERE r_requester = '$userid'";
				$count = ($reqresult = mysqli_query($con, $reqsql))?mysqli_num_rows($reqresult):0;
				echo $count;
			?></a><br/>
			<a href="subscriptions.php?type=solution"><em>Number of Solutions:</em> <?php
				$userid = $_SESSION['id'];
				$reqsql = "SELECT r_id FROM requests WHERE r_contributor = '$userid' and r_status = 'SOLVED'";
				$count = ($reqresult = mysqli_query($con, $reqsql))?mysqli_num_rows($reqresult):0;
				echo $count;
			?></a><br/>
			<br/>			<!-- <a href="subscriptions.php?type=subcribe"><em>Subcriptions</em></a> -->
			<br />
			<br/>
<!-- <input type="submit" name="action" value="Log Out"/> -->
		</form>
	<?php
	}
?>
<hr>
<!-- Project Section -->
<?php
	include('database.php');
	$user_id = $_SESSION['id'];
	// find projects that user has selected
	
	$titleError = "";
	$pTitle = "";
	$created = false;
	// php handling of new project requests
	if($_SERVER["REQUEST_METHOD"] == "POST"&&!empty($_POST['action'])){
		$action = $_POST['action'];
		if($action == "selectAction"){
			if(!empty($_POST['project'])){
                foreach($_POST['project'] as $projectID){
                    //echo "$report_id was checked! ";
                    $projectsuserssql = "INSERT INTO projectsusers (p_id, u_id) VALUES ('$projectID', '$user_id')";
                    mysqli_query($con,$projectsuserssql);
                }
   			}
		}

		if($action == "deleteAction"){
			if(!empty($_POST['project'])){
                foreach($_POST['project'] as $projectID){
                    //echo "$report_id was checked! ";
                    $projectsuserssql = "DELETE from projectsusers where p_id ='$projectID' AND u_id =  '$user_id'";
                    mysqli_query($con,$projectsuserssql);
                }
   			}
		}

		if($action == 'abolishAction'){
			if(!empty($_POST['project'])){
				foreach($_POST['project'] as $projectID){
					$projectsuserssql = "DELETE from projects where p_id ='$projectID'";
					mysqli_query($con,$projectsuserssql);
				}
			}
		}
	}



	$projectsql = "SELECT * FROM projectsusers as pu INNER JOIN projects as p ON pu.p_id = p.p_id WHERE pu.u_id = '$user_id'";
	
	$count = ($projectresult = mysqli_query($con, $projectsql))?mysqli_num_rows($projectresult):0;


	if($_SERVER["REQUEST_METHOD"] == "POST"&&empty($_POST['action'])){
		if(empty($_POST["p_title"])){
			$titleError = "Project title is required";
		}
		else{
			$ptitle = trim($_POST["p_title"]);
			if (!preg_match("/^[a-zA-Z ]*$/",$ptitle)) {
				$fnameError = "Only letters and whitespace allowed"; 
			}
		}
		$listsql = "SELECT * FROM projects";
		$listresult = mysqli_query($con, $listsql);
		
		while($listrow = mysqli_fetch_assoc($listresult)){
			if($ptitle == $listrow['p_title']){
				$titleError = "That project already exists";
				break;
			}
		}
		// process new project request if there are no errors
		if ($titleError == ""){
			$createsql = "INSERT INTO projects (p_id, p_title) VALUES (NULL, '$ptitle');";
			mysqli_query($con,$createsql);
			$lastsql = "SELECT p_id FROM projects ORDER BY p_id DESC LIMIT 1;";
			$lastresult = mysqli_query($con,$lastsql);
			while($lastrow = mysqli_fetch_assoc($lastresult)){
				$projectId = $lastrow['p_id'];
			}
			$selectsql = "INSERT INTO projectsusers (p_id, u_id) VALUES ('$projectId', '$user_id');";
			mysqli_query($con,$selectsql);
			echo "Project created and selected. <br/> Page will be refreshed now. <br/>";
			header( "refresh:5;url=index.php" );
			echo "<a href=\"index.php\">Click here if page does not refresh in 5 seconds.</a><br/>";			
		}
	}
	// if user has selected projects, and is not requesting a new project
	if($count > 0 && $created == false){
		?>
		<h2> Available Actions: </h2>
		<p>To Create a new request please click on the corresponding Project</p>
		<?php if(isset($_SESSION['loggedin'])){ ?>
		<input type="button" onclick="location.href='projectlist.php'" value="Select a Project"/> 
		<input type="button" onClick='createProject();' value="Create a New Project"/>
    	<input type="button" onclick="location.href='delete_projects.php'" value="Unselect a Project"/>
    	<input type="button" onclick="location.href='abolish.php'" value="Delete Project"/>
    	<?php }?>
		<h2>Your selected projects:</h2>
		<br/>
		<table style="width:100%">
			<tr>
				<th>Project ID</th>
				<th>Project Title</th>
			</tr>
			<?php while($row = mysqli_fetch_assoc($projectresult)){?>
				<tr>
					<td><?php echo $row["p_id"];?></td>
					<td><a href="requests.php?id=<?php echo $row["p_id"];?>"><?php echo $row["p_title"];?></a></td>
				</tr>
			<?php }?>
		</table>
		<br/><br/>
		<?php
	}
	else{
		?>
		<h2>You have not selected any projects</h2>
		<br/><br/>
	<?php }?>
	<br/><br/>
	<div class="creation" id="createProject">
	<?php if($_SERVER["REQUEST_METHOD"] == "POST" && $created == false && empty($_POST['action'])){
		if(isset($_POST['p_title'])){
			$pTitle = trim($_POST['p_title']);
		}?>
		<script type="text/javascript">
			createProject();
		</script>
	<?php }?>

			<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
				<h2>Create a new project:</h2>
				<br/><br/>
				Project Title: <input type="text" name="p_title" value="<?php echo $pTitle ?>"/>
				<span class="error"><?php echo $titleError;?></span>
				<br/><br/>
				<input type="submit" value="Create Project">
				<input type="button" value="Cancel" OnClick="window.location.href='index.php'">  </input>
			</form>
			</div>
	
	<br/><br/>

<!-- Projection Section End -->


<?php include('endbit.inc'); ?>