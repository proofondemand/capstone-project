<?php include('topbit.inc'); ?>
<title>Project</title>
<?php include('middlebit.inc'); ?>
<?php
	include('database.php');
	
	$userid = $_SESSION['id'];
	$projectid = $_GET['id'];
	define ('SITE_ROOT', realpath(dirname(__FILE__)));	
	$new = "false";  


	$reqsql = "SELECT r.r_id, r.r_requester, u1.u_fname as r_reqfname, u1.u_lname as r_reqlname, r.r_reqdate, 
	r.r_lemmatitle, r.r_lemma, r.r_parenttitle, r.r_parentname, r.r_status, r.r_contributor, u2.u_fname as r_confname, 
	u2.u_lname as r_conlname, r.r_condate, r.r_comment, r.r_moddate 
	FROM requests as r LEFT OUTER JOIN users as u1 ON r.r_requester = u1.u_id 
	LEFT OUTER JOIN users as u2 ON r.r_contributor = u2.u_id 
	INNER JOIN projects as pr ON r.r_project = pr.p_id WHERE r.r_project = '$projectid'";
	
	$reqcount = ($reqresult = mysqli_query($con, $reqsql))?mysqli_num_rows($reqresult):0;

	
	$lemmaError = $pTitleError = $pNameError = $finalError = "";
	$pTitleIndex = $pNameIndex = 0;
	$lemmaTitle = $lemma = $newTitle = $newName = "";
	$created = false;
	
	$commentError = $editError = $deleteError = $confirmationMessage = "";
	// function to check for requests with contradictions
	function contradiction_check($con){
		$contradictions = array();
		$contchecksql = "SELECT r_id, r_parentname FROM requests WHERE r_status = 'CONTRADICTION'";
		$contcheckcount = ($contcheckresult = mysqli_query($con, $contchecksql))?mysqli_num_rows($contcheckresult):0;
		if($contcheckcount > 0){
			while($contcheckrow = mysqli_fetch_assoc($contcheckresult)){
				$cont_id = $contcheckrow["r_parentname"];
				// insert id of requests with contradictions into array
				array_push($contradictions, $cont_id);
			}
		}
		return $contradictions;
	}
	// recursive function to change status of requests that are dependent on requests with contradictions, into "CONTRADICTION"
	function set_contparents($con_id, $con){
		$findparentsql = "SELECT r.r_id, r.r_parentname, r.r_status, r.r_comment FROM requests AS r INNER JOIN parentnames AS pn ON r.r_id = pn.r_id WHERE pn.pn_id = '$con_id'";
		$findparentcount = ($findparentresult = mysqli_query($con, $findparentsql))?mysqli_num_rows($findparentresult):0;
		if($findparentcount > 0){
			// take details of the parent request that has a contradiction
			while($findparentrow = mysqli_fetch_assoc($findparentresult)){
				$contparentid = $findparentrow["r_id"];
				$contparparent = $findparentrow["r_parentname"];
				$contparentstatus = $findparentrow["r_status"];
				$contparentcomment = $findparentrow["r_comment"];
			}
			// if the request that has a contradiction also has another request that is dependent on it...
			if($contparentid != "NULL"){
				// if the dependent request's status is already on contradiction, and there is no relevant message...
				if($contparentstatus === "CONTRADICTION" && strpos($contparentcomment, "This lemma is dependent on a lemma with a contradiction") === false){
					$contparentcomment .= "\n\nThis lemma is dependent on a lemma with a contradiction";
					// add relevant message to dependent request's comments
					$contparenteditsql = "UPDATE requests SET r_comment='$contparentcomment' WHERE r_id='$contparentid'";
					mysqli_query($con, $contparenteditsql);
				}
				// if the dependent request's status is not "CONTRADICTION"...
				elseif($contparentstatus !== "CONTRADICTION"){
					// change status and add relevant message
					$contparenteditsql = "UPDATE requests SET r_status='CONTRADICTION', r_comment='This lemma is dependent on a lemma with a contradiction' WHERE r_id='$contparentid'";
					mysqli_query($con, $contparenteditsql);
				}
				if($contparparent != "NULL"){
					// recursive function call. if the dependent request is parent to another request, call this function on that request as well
					set_contparents($contparparent, $con);
				}
			}
		}
	}

	function check_subscribed($r_id, $con)
	{
		
		$subscribed = false;
		$subcheckcount = 0;
		$subSQL = "SELECT * FROM usersrequests WHERE r_id = ".$r_id." AND u_id = ".$_SESSION['id']."";
		//echo $subSQL;
	
		if ($result = mysqli_query($con, $subSQL))
		{
			$subcheckcount = mysqli_num_rows($result);
			if($subcheckcount > 0){
				//echo "Row found";
				$subscribed = true;
			}
		}
		else
		{
			echo 'Error with connection';
		}
		
		return $subscribed;
	}
	/***** OLD UPDATE CODE. REPLACED WITH AJAX. LEFT COMMENTED FOR REFERENCE *********/
	/*
	if($_SERVER["REQUEST_METHOD"] == "POST" && isset($_GET['action'])){
		// php handling of edit status request
		if($_GET['action'] == "edit" && isset($_GET['editid'])){
			$reqID = $_GET['editid'];
			$newStatus = $_POST["newStatus"];
			$newComment = trim($_POST["newComment"]);
			if ($newStatus == "SOLVED" || $newStatus == "CONTRADICTION"){
				if ($newComment == ""){
					$commentError = "Please enter solution or contradiction in comment box";
				}
			}
			if ($commentError == ""){
				// if no errors in edit status request...
				if($newStatus == "REQUESTED"){
					$editsql = "UPDATE requests SET r_status='$newStatus', r_contributor=NULL, r_condate=NULL, r_comment=NULL WHERE r_id='$reqID'";
				}
				else{
					$editsql = "UPDATE requests SET r_status='$newStatus', r_contributor='$userid', r_condate=NOW(), r_comment='$newComment' WHERE r_id='$reqID'";
					$subscribesql = "INSERT INTO usersrequests (u_id, r_id) VALUES ('$userid', '$reqID');";
				}
				try{
					mysqli_query($con, $subscribesql);
					mysqli_query($con, $editsql);
					// send email notification to supervisor. change supervisor's email address and other inputs as necessary
					$emailAddress = "anooooos-2oo9@hotmail.com"; // the email address we are sending to
					$subject = "Changes"; // the title of the email
					$message = "the status of the project has been changed"; // the message we are sending

					if(mail($emailAddress,$subject,$message))
					{
						$confirmationMessage = "Supervisor has been notified of changes.";
					}
					else
					{
						$confirmationMessage = "Supervisor was NOT notified. Email system broken.";
					}
					$reqcount = ($reqresult = mysqli_query($con, $reqsql))?mysqli_num_rows($reqresult):0;
				}
				catch(Exception $e){
					$editError = "Error editing request";
				}
			}
		}
	}
	*/
	if(isset($_GET['action'])){
		// php handling of deletion requests
		if($_GET['action'] == "delete" && isset($_GET['deleteid'])){
			$reqID = $_GET['deleteid'];
			$checksuccess = false;
			// check if user that made request is really the requester
			$checksql = "SELECT r_id, r_requester FROM requests WHERE r_requester = '$userid'";
			$checkcount = ($checkresult = mysqli_query($con, $checksql))?mysqli_num_rows($checkresult):0;
			if($checkcount > 0){
				while($checkrow = mysqli_fetch_assoc($checkresult)){
					if($checkrow["r_id"] == $reqID){
						$checksuccess = true;
						break;
					}
				}
			}
			if($checksuccess == true){
				$deletesql = "DELETE FROM requests WHERE r_id = '$reqID'";
				$mask = $reqID.'_*.*';
                array_map('unlink', glob($mask));

				try{
					mysqli_query($con, $deletesql);
					// send email notification to supervisor. change supervisor's email address and other inputs as necessary
					$emailAddress = "anooooos-2oo9@hotmail.com"; // the email address we are sending to
					$subject = "Delete"; // the title of the email
					$message = "The project has been deleted"; // the message we are sending

					if(mail($emailAddress,$subject,$message))
					{
						$confirmationMessage = "Supervisor has been notified of changes.";
					}
					else
					{
						$confirmationMessage = "Supervisor was NOT notified. Email system broken.";
					}
				}
				catch(Exception $e){
					$deleteError = "Error deleting request";
				}
			}
			else{
				$deleteError = "Cannot delete a request that isn't yours"; 
			}
		}
	}
	?>
	<script type="text/javascript">
		// javascript function to pre-populate dropdown boxes for purposes of creating a new request
		// get rid of this function and replace with AJAX/jQuery/etc. in future improvements
		function clearBox(selectbox){
			for (i = selectbox.options.length-1;i>=2;i--){
				selectbox.remove(i);
			}
		}
		function fillBox(){
			inputParentTitle();
			var selected = document.getElementById("parentTitle").value;
			console.log(selected);
			var id = "";
			for (i = 0; i < titles.length; i++){
				if(selected === titles[i]){
					id = titles[i];
					break;
				}
			}
			console.log(id);
			if(id !== ""){
				var names = document.getElementById("parentName");
				clearBox(names);
				var varname = "id_"+id;
				for (j = 0; j < eval(varname).length; j++){
					var idname = eval(varname)[j];
					console.log(idname);
					var idname = idname.split("|");
					var nameID = idname[0];
					var name = idname[1];
					var option = document.createElement("option");
					option.text = name;
					option.value = nameID;
					names.appendChild(option);
				}
			}
		}
	</script>
	<?php 
	
	// if new request creation failed, insert old input data into new request form
	if($_SERVER["REQUEST_METHOD"] == "POST" && $created == false){
		if(isset($_POST['lemma'])){
			$lemma = trim($_POST['lemma']);
		}
		if(isset($_POST['newTitle'])){
			$newTitle = trim($_POST['newTitle']);
		}
		if(isset($_POST['newName'])){
			$newName = trim($_POST['newName']);
		}
	}
	// set variables for different row colours in css
	$colors = array(
		"CONTRADICTION" => "red",
		"SOLVED" => "limegreen"
	);
	
	if($reqcount > 0){
		// call function to check contradiction and change status of contradictory parent requests
		$contradictions = contradiction_check($con);
		foreach($contradictions as $con_id){
			set_contparents($con_id, $con);
		}
		?>
		<?php
			$ptitle = "SELECT p_title FROM projects WHERE p_id = '$projectid'";
			$result = mysqli_query($con, $ptitle); 
			$titleResult = mysqli_fetch_assoc($result);
		 ?>
		<div class="requests">
			The Project: <?php  echo $titleResult["p_title"]; ?> contains the following request: <br />
		<?php echo $editError."<br/>";?>
		<table style="width:100%">
			<tr>
				<th>Request from</th>
				<th>Lemma</th>
				<th>Used in</th>
				<th>Status</th>
			</tr>
			<?php while($row = mysqli_fetch_assoc($reqresult)){
					
					$date1 = new DateTime($row["r_reqdate"]);
					$date2 = new DateTime(null);
					$dateDiff = $date1->diff($date2);
					if($dateDiff->format('%a') <= 1){
						$new = "true"; 
					}
					else{
						$new = "false"; 
					}
					
				
				?>
				<!-- set background colour css of table row depending on request's status -->
				<tr style="background-color: <?php echo (isset($colors[$row["r_status"]])?$colors[$row["r_status"]]:"white"); ?>">
					<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]."?id=".$projectid."&action=edit&editid=".$row["r_id"]); ?>">
						<td><?php echo $row["r_reqfname"]." ".$row["r_reqlname"];?> <br/>
							<?php 
							if($new == "true")
							{
								echo $row["r_reqdate"]."</br></br> <strong>This is a New Request</strong>";
								
							}
							else{
								echo $row["r_reqdate"];
							}?></td>
						<td><pre>lemma <?php echo htmlspecialchars($row["r_lemmatitle"]);?>:</pre><pre><?php echo htmlspecialchars($row["r_lemma"]);?></pre><br/><br/>Last modified date: <em><?php echo $row["r_moddate"];?></em></td>
						<td><pre><?php echo $row["r_parenttitle"];?>:<br/><?php echo $row["r_parentname"];?></pre></td>
						<td><span id="status<?php echo $row["r_id"];?>"><?php echo $row["r_status"];?></span>
						<span id="statusin<?php echo $row["r_id"];?>" class="editArea">
						<select name="newStatus" id="newStatus_<?php echo $row["r_id"];?>" onChange="updateStatus('<?php echo $row["r_id"];?>')">
							<option value="REQUESTED" <?php echo ($row["r_status"]=="REQUESTED")?('selected'):('');?>>REQUESTED</option>
							<option value="IN PROGRESS" <?php echo ($row["r_status"]=="IN PROGRESS")?('selected'):('');?>>IN PROGRESS</option>
							<option value="SOLVED" <?php echo ($row["r_status"]=="SOLVED")?('selected'):('');?>>SOLVED</option>
							<option value="CONTRADICTION" <?php echo ($row["r_status"]=="CONTRADICTION")?('selected'):('');?>>CONTRADICTION</option>
						</select>
						</span>
						<br/><?php echo ($row["r_confname"]===NULL&&$row["r_conlname"]===NULL)?(''):($row['r_confname'].' '.$row['r_conlname'].'<br/>'.date('d/m/Y h:i A', strtotime($row['r_condate'])));?><br/><br/><span id="comment<?php echo $row["r_id"];?>"><pre><?php echo ($row['r_comment']===NULL)?(''):($row["r_comment"]);?></pre></span>
						<span id="commentin<?php echo $row["r_id"];?>" class="editArea">
						Comments:<br/>
						<textarea name="newComment" id="comment_<?php echo $row["r_id"];?>" onBlur="saveComment('<?php echo $row["r_id"];?>')"><?php echo ($row["r_comment"]===NULL)?(''):($row["r_comment"]);?></textarea>
						</span>
						<br/><br/><div id="editin<?php echo $row["r_id"];?>" class="editButtons"><!-- <input type="submit" value="Save Changes"/> --></div>
						<?php if(isset($_SESSION['loggedin'])){ ?>
							<div class="requestButtons">
								<input type="button" value="Edit" onClick='changeStatus(<?php echo $row["r_id"];?>)'/>
								<?php if($_SESSION['id'] == $row["r_requester"]){?> 
									<input type="button" value="Delete Request" onClick='deleteRequest(<?php echo $projectid.",".$row["r_id"];?>)'/>
								<?php } ?> 
									
										<?php if(check_subscribed($row['r_id'], $con)==false) { ?>
											<button onclick="subscribe(<?php echo $row['r_id']; ?>, '<?php echo htmlspecialchars($row["r_lemmatitle"]);?>')" >Subscribe</button>
										<?php } else { ?>
											<button onclick="unsubscribe(<?php echo $row['r_id']; ?>)" >Unsubscribe</button>
										
								<?php }?>
							</div></td>
						<?php } ?>
					</form>
				</tr>
			<?php }?>
		</table>
		</div>
		<br/>
		<?php if(isset($_SESSION['loggedin'])){ ?>
			<a href=<?php echo "new_request.php?id=".$projectid;?> >Add New Request</a>
			<input type="button" value="Cancel" OnClick="window.location.href='index.php'">  </input>
		<?php } ?>
	<?php }
	else{?>
	<h4>No requests in this project.<a href=<?php echo "new_request.php?id=".$projectid;?> >Click here to add new Request</a></h4>
	<input type="button" value="Cancel" OnClick="window.location.href='index.php'">  </input>
	<?php }?>
	<br/><br/>
	<?php if($_SERVER["REQUEST_METHOD"] == "POST" && $created == false && !isset($_GET['action'])){
		if(isset($_POST['p_title'])){
			$pTitle = trim($_POST['p_title']);
		}?>
		<script type="text/javascript">
			createRequest();
		</script>a
	<?php }?>
	<!-- form for creating a new request -->

	<?php if($_SERVER["REQUEST_METHOD"] == "POST" && $created == false){
		if($pTitleIndex==1){?>
			<script type="text/javascript">
				inputParentTitle();
			</script>
		<?php }
		if($pNameIndex==1){?>
			<script type="text/javascript">
				inputParentName();
			</script>
		<?php }
	}?>
	<?php 
		// recursive function to get dependency tree in text format
		function get_children($par_id, $par_title, $con, &$text){
			// find requests that are dependent on above request (par_id)
			$childrensql = "SELECT r_id, r_lemmatitle, r_parentname FROM requests WHERE r_parentname = '$par_id'";
			$childrencount = ($childrenresult = mysqli_query($con, $childrensql))?mysqli_num_rows($childrenresult):0;
			if($childrencount > 0){
				$text .= "[";
				$loopcount = 0;
				// loop through each dependent request
				while($childrow = mysqli_fetch_assoc($childrenresult)){
					// record relevant data from dependent requests
					$childreq_id = $childrow["r_id"];
					$childlemmatitle = $childrow["r_lemmatitle"];
					// find dependent request in parentnames table
					$childinparentsql = "SELECT pn_id, pn_name FROM parentnames WHERE pn_name = '$childlemmatitle'";
					$childinparentresult = mysqli_query($con, $childinparentsql);
					while($childinparentrow = mysqli_fetch_assoc($childinparentresult)){
						$childinparentid = $childinparentrow["pn_id"];
					}
					// find requests that are dependent on the dependent request
					$childchildrensql = "SELECT r_lemmatitle, r_parentname FROM requests WHERE r_parentname = '$childinparentid'";
					$childchildrencount = ($childchildrenresult = mysqli_query($con, $childchildrensql))?mysqli_num_rows($childchildrenresult):0;
					if($childchildrencount > 0){
						$text .= $childlemmatitle;
						// recursive function call on request that is dependent on the dependent request
						get_children($childinparentid, $childlemmatitle, $con, $text);
					}
					else{
						$text .= $childlemmatitle;
						if($loopcount+1 < $childrencount){
							$text .= ", ";
						}
					}
					$loopcount++;
				}
				$text .= "]";
				return $text;
			}
			else{
				return "";
			}
		}
	?>
	<br/>
	<!-- twitter and facebook sharing code -->
	<a href="https://twitter.com/share" class="twitter-share-button" data-text="Check this out" data-size="large">Tweet</a>
	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
	<div class="fb-share-button" data-href="http://localhost/Anas/requests.php?id=1" data-layout="button_count"></div>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.3";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	<br/>
<?php include('endbit.inc'); ?>