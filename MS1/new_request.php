
<?php 
include('topbit.inc');
include('excel_top.inc'); 
include('string_util.inc'); 
?>

<title>New Request</title>
<?php include('middlebit.inc'); ?>

<?php
	include('database.php');
	define ('SITE_ROOT', realpath(dirname(__FILE__)));	

	$userid = $_SESSION['id'];
	$projectid="";
	if(isset($_GET['id'])){
		$projectid = $_GET['id'];
    }	

    unset($_SESSION['pre_lemmas']);
	$lemmaError = $pTitleError = $pNameError = $finalError = "";

	$excelError = "";
	$created = isset($_POST['created'])?$_POST['created']:false;
	$uploaded = isset($_POST['uploaded'])?$_POST['uploaded']:false;
	
	$analyzed = false;
	$uploaddir = SITE_ROOT.'/files/lemma_excel/';

	$pre_lemmas = array(); 
	$parentTitle = $parentName = "";

	$lemma_titles_excel = array();

	if(!empty($_FILES['userfile'])&&!$uploaded){
	    $_SESSION['lemmaTitle'] = $lemmaTitle = $_POST['lemmaTitle'];
        $_SESSION['lemma'] = $lemma = $_POST['lemma'];

		$filename = basename($_FILES['userfile']['name']);
		$filelen=strpos($filename,'.');
		$_SESSION['parentTitle'] = $parentTitle = substr($filename, 0, $filelen); 

		if(!endsWith($filename, ".xls")&&!endsWith($filename,".xlsx")){
		    $excelError = "File Type Error.";
	    }
	    else{
			$uploadfile = $uploaddir.'excel_tmp.xlsx';
			$lemma_excel_file = $_FILES['userfile']['tmp_name'];

			$objReader = PHPExcel_IOFactory::createReaderForFile($lemma_excel_file);
			$objPHPExcel = $objReader->load($lemma_excel_file);
	        $objPHPExcel->setActiveSheetIndex(3);
	        $objWorksheet = $objPHPExcel->getActiveSheet();

	        $row_count = $objWorksheet->getHighestRow();
	        $col_count = $objWorksheet->getHighestColumn();

	        

	        $hasLemma = false;
	        for($i = 1; $i <=$row_count; $i++){
        		if(strtolower($objWorksheet->getCellByColumnAndRow(0, $i)->getValue())==strtolower($_POST['lemmaTitle'])){
            		$hasLemma = true;
        	 		
        	 		if(!file_exists($uploadfile)){
        	 			move_uploaded_file($lemma_excel_file, $uploadfile);
        	 		}
        	 		//echo 'Found in row: '.$i;
        	 		//while($objWorksheet->cellExistsByColumnAndRow($j,$i)){
        	 		

        	 		
        	 	}
        	 	$lemma_titles_excel[] = $objWorksheet->getCellByColumnAndRow(0, $i)->getValue();

        	 	for($j = 1; $j < 5; $j++) {
        	 			if($objWorksheet->getCellByColumnAndRow($j, $i)->getValue() != ''){
       	 					$_SESSION['pre_lemmas'][$objWorksheet->getCellByColumnAndRow(0, $i)->getValue()][] = $objWorksheet->getCellByColumnAndRow($j, $i)->getValue();
       	 					//echo 'Row:'.$i.' Col:'.$j.' -'.$objWorksheet->getCellByColumnAndRow($j, $i)->getValue().'<br/>';
       	 				}
        	 			//$pre_lemmas[] = $objWorksheet->getCellByColumnAndRow($j, $i)->getValue();
        	 		 	
        	 		 	
        	 		}
        	 	
        	 	
        	}

        	 if($hasLemma == false){
        	 	//$finalError = "Cannot find the given lemma in theory file.";
        	 }



        $analyzed = true;
    }
}

	   if($_POST['manual'] == "Submit"){
	   	$_SESSION['lemmaTitle'] = $lemmaTitle = $_POST['lemmaTitle'];
        $_SESSION['lemma'] = $lemma = $_POST['lemma'];
        $_SESSION['parentTitle'] = $parentTitle = $_POST['parentTitle'];
        $_SESSION['parentName'] = $parentName = $_POST['parentName'];

        	$requestsql = "INSERT INTO requests (
			 	r_id, r_project,     r_requester, r_reqdate, r_lemmatitle,                  r_lemma,                r_parenttitle,               r_parentname,                r_status,    r_contributor, r_condate, r_comment, r_moddate) VALUES 
				(NULL, '$projectid', '$userid',   NOW(),     '".$_SESSION['lemmaTitle']."', '".$_SESSION['lemma']."'  , '".$_SESSION['parentTitle']."' , '".$_SESSION['parentName']."' , 'REQUESTED', NULL, NULL, NULL, NOW());";
            if(!mysqli_query($con, $requestsql)){
            	echo "Error: " . $requestsql . "<br>" . mysqli_error($con);
            }
            else{
                echo "Success";
                header("Location:index.php");
            }
        }

	if($uploaded){
		if($created){
			$requestsql = "INSERT INTO requests (
			 	r_id, r_project,     r_requester, r_reqdate, r_lemmatitle,                  r_lemma,                r_parenttitle,               r_parentname,                r_status,    r_contributor, r_condate, r_comment, r_moddate) VALUES 
				(NULL, '$projectid', '$userid',   NOW(),     '".$_SESSION['lemmaTitle']."', '".$_SESSION['lemma']."'  , '".$_SESSION['parentTitle']."' , '".$_SESSION['parentName']."' , 'REQUESTED', NULL, NULL, NULL, NOW());";

            if(!mysqli_query($con, $requestsql)){
            	echo "Error: " . $requestsql . "<br>" . mysqli_error($con);
            }
            else{
                rename(SITE_ROOT.'/files/lemma_excel/excel_tmp.xlsx', SITE_ROOT.'/files/lemma_excel/'.mysql_insert_id()."_".$_SESSION['parentTitle'].'.xlsx');
            }

  


        }
        else{
        	$usersql = "SELECT u_fname, u_lname FROM users WHERE u_id = '$userid'";
        	$userresult = mysqli_query($con, $usersql);
        	$namerow = mysqli_fetch_assoc($userresult);
        	$username = $namerow['u_fname']." ".$namerow['u_lname'];


        	$requestsql = "UPDATE requests 
        	SET r_moddate = NOW(), r_comment = CONCAT(IfNull(r_comment,''), '\n\n Requested by  $username at ',NOW())
        	WHERE r_project = '$projectid' AND r_lemmatitle = '".$_SESSION['lemmaTitle']."';";

        	if(!mysqli_query($con, $requestsql)){
            	echo "Error: " . $requestsql . "<br>" . mysqli_error($con);
            }

        	unlink(SITE_ROOT.'/files/lemma_excel/excel_tmp.xlsx');


        }
        unset($_SESSION['lemmaTitle']);
        unset($_SESSION['lemma']);
        unset($_SESSION['parentTitle']);
        unset($_SESSION['parentName']);
   	}

?>

<body>
	<p style="text-align:center">You can add request data manually or using data from the corresponding excel file</p>

	
	
	<div style="width:80%; margin:0 auto; text-align:center">
	<span>Lemma:</span><br/>
	<form  action=<?php echo "new_request.php?id=".$projectid; ?> method="POST" id="requestForm">
	<textarea style="width: 400px; height: 150px;" name="lemma" required ><?php echo isset($lemma)?$lemma:'' ?></textarea>

	<table style="width:100%">
		
		<tr>
			
			<td style="vertical-align:top;">
				<h2 id="manual_head">Enter Information Manually:</h2>
				

					Lemma Title:&nbsp; &nbsp; <input type="text" name="lemmaTitle" id="lemmaTitle" value="<?php echo isset($lemmaTitle)?$lemmaTitle:'' ?>" required/>
					<br/>
					<br/>
					
					<br />
					
					<br />
					<br />
					Theory File:
					<input type="text" name="parentTitle" id="parentTitle" value="<?php echo isset($parentTitle)?$parentTitle:'' ?>" />
					<br />
					<br />
					Used in:<br />
					<span id="message"></span>
					<textarea style="width: 400px; height: 150px;" name="parentName" id="parentName" ></textarea>
					<br />
					<br />
					<input type="submit" value="Submit" name="manual" id="input" onclick="index.php"/>
					<br />
					<br />
				</form>
			</td>

			<td style="vertical-align:top;">
				<h2>Import From Excel:</h2>
					<form enctype="multipart/form-data" action=<?php echo "new_request.php?id=".$projectid; ?> method="POST">
					<!-- MAX_FILE_SIZE must precede the file input field -->
					<input type="hidden" name="MAX_FILE_SIZE" value="30000" />
					<!-- Name of input element determines name in $_FILES array -->
					Select excel file: 
					<input name="userfile" type="file" value="Browse"/>
					<input type="submit" value="Upload File" />
					<br />
					<br/>
					
						Lemma Title:&nbsp; &nbsp; <!--input type="text" name="lemmaTitle" value="<?php //echo isset($lemmaTitle)?$lemmaTitle:'' ?>" required/-->
					
					
						<select onChange="updateTitle(this);">
							<option>< Lemma Title ></option>
							<?php foreach($lemma_titles_excel as $lt){ ?>
								<option value="<?php echo $lt?>"><?php echo $lt; ?></option>
							<?php } ?>
						</select>

						
						<?php 
						
						//print_r($_SESSION['pre_lemmas']);
						//print_r($pre_lemmas);
						/*foreach ($_SESSION['pre_lemmas'] as $pre) {

							//print_r($pre);
							//echo '<br/>';
						}
						
						foreach ($_SESSION['pre_lemmas'] as $i => $values) {
						    print "$i {\n";
						    foreach ($values as $key => $value) {
						        print "    $key => $value<br/>";
						    }
						    print "}\n";
						}
							*/ ?>
					
				</form>
			</td>
		</tr>
	</table>



<form method="post" action=<?php echo "new_request.php?id=".$projectid; ?>>
		<span class="error"><?php echo $finalError;?></span>
	<br/><br/>
	<?php 
		if($analyzed&&$finalError==""){?>
			<?php
			if(!empty($pre_lemmas)){
				echo "Used in:&nbsp; &nbsp;";

			    foreach($pre_lemmas as $pre_lemma){
			    	echo $pre_lemma."   ";
			    }
			    $_SESSION['parentName'] = implode('\n',$pre_lemmas);

			    //print_r($_SESSION['parentName']);
			}
			else{
				echo "The Lemma is not used in any other lemmas.<br />";
			}

    	    $requestsql = "SELECT r_id FROM requests WHERE r_project = '".$projectid."' AND r_lemmatitle = '".$lemmaTitle."';";
	        if($requestresult = mysqli_query($con,$requestsql)){
	        	if(mysqli_num_rows($requestresult) != 0){
	        		echo "<h5>The lemma has been requested in this Project.</h5>";
	        		echo '<br /><br /><input type="submit" name="submit" value="UPDATE REQUEST">';
	        		echo '<input type="hidden" name="uploaded" value="1" />';
    	            echo '<input type="hidden" name="created" value="0" />'; 
	        	}
	        	else{
    	           echo '<br /><br /><input type="submit" name="submit" value="CREATE REQUEST">';
    	           echo '<input type="hidden" name="uploaded" value="1" />';
    	           echo '<input type="hidden" name="created" value="1" />'; 
    	       }
	        }
		}
		else{
			?>
			<h4></h4>
            <br />
		    <br />
		<?php


		}
	?>

</form>

</body>

<?php include('endbit.inc'); ?>