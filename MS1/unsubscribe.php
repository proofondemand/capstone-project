<?php
session_start();

$userid = $_SESSION['id'];
$reqID = $_POST['request'];

include('database.php');

	try{
        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                                   // set the PDO error mode to excepti
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $sql = "DELETE FROM usersrequests WHERE u_id = :userid AND r_id = :reqID";

		$stm = $conn->prepare($sql);
		
		$stm->bindParam(':userid', intval($userid), PDO::PARAM_INT);
		$stm->bindParam(':reqID', intval($reqID), PDO::PARAM_INT);


		$stm->execute();

		echo 'Unsubscribed';
	}
	 catch(PDOException $e)
     {
         echo $sql . "<br/>" . $e->getMessage();
     }
       

?>