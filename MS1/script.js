function createProject() {
	document.getElementById("createProject").style.display="block";
}

function createRequest() {
	document.getElementById("createRequest").style.display="block";
}

function inputParentTitle() {
	if(document.getElementById("parentTitle").value == "createNewTitle"){
		document.getElementById("inputParentTitle").style.display="inline";
	}
	else{
		document.getElementById("inputParentTitle").style.display="none";
	}
	if(document.getElementById("parentTitle").value != ""){
		document.getElementById("parentNameArea").style.display="inline";
	}
	else{
		document.getElementById("parentNameArea").style.display="none";
	}
}

function inputParentName() {
	if(document.getElementById("parentName").value == "createNewName"){
		document.getElementById("inputParentName").style.display="inline";
	}
	else{
		document.getElementById("inputParentName").style.display="none";
	}
}

function changeStatus(id) {
	$("#statusin"+id).toggle();//.style.display="inline";
	$("#commentin"+id).toggle();//.style.display="inline";
	$("#editin"+id).toggle();//.style.display="inline";
	$("#status"+id).toggle();//.style.display="none";
	$("#comment"+id).toggle();//.style.display="none";
}

function deleteRequest(projectid, requestid){
	var c = confirm("Are you sure you want to delete this request?");
	if(c == true){
		
		window.location = "requests.php?id="+projectid+"&action=delete&deleteid="+requestid;
		window.reload();
		
	}
}
function subscribe(id)
{
	$.ajax({
		        type: 'POST',
		        url: "subscribe.php",
		        data: {
		        		request:id
		        },
		        success: function(result) {
		           
		           alert(result);
		        },
		        error: function(xhr,textStatus,error){
               //         alert(textStatus);
		        }
		        //dataType: 'json',
		        
		    });
}

function unsubscribe(id)
{
	$.ajax({
		        type: 'POST',
		        url: "unsubscribe.php",
		        data: {
		        		request:id
		        },
		        success: function(result) {
		           
		           alert(result);
		        },
		        error: function(xhr,textStatus,error){
                        alert(textStatus + error);
		        }
		        //dataType: 'json',
		        
		    });	
}

function updateStatus(id)
{
	var status = $('#newStatus_' + id).val();
	//console.log(selected);
	$.ajax({
		        type: 'POST',
		        url: "update_status.php",
		        data: {
		        		request:id,
		        		status:status
		        },
		        success: function(result) {
		           
		       //    alert(result);
		        },
		        error: function(xhr,textStatus,error){
                    //    alert(textStatus);
		        }
		        //dataType: 'json',
		        
		    });
	$('#status' + id).html(status);

}
function saveComment(id)
{
	var comment = $('#comment_' + id).val();
	//console.log(selected);
	$.ajax({
		        type: 'POST',
		        url: "update_comment.php",
		        data: {
		        		request:id,
		        		comment:comment
		        },
		        success: function(result) {
		           console.log(result);
		       //    alert(result);
		        },
		        error: function(xhr,textStatus,error){
                    //    alert(textStatus);
		        }
		        //dataType: 'json',
		        
		    });
	$("#comment"+id).html(comment);

}
function showUser(str) {
    if (str == "") {
        document.getElementById("txtHint").innerHTML = "";
        return;
    } else { 
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
            }
        };
      	window.location = "subscriptions.php?type="+str;
    }
}
function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}


function updateTitle(data)
{
	$('#lemmaTitle').val(data.value);
	$('#lemmaTitle').attr('readonly', true);
	$('#parentTitle').attr('readonly', true);
	$('#parentName').attr('readonly', true);
	$('#manual_head').html('Imported Lemma Info:');
	$('#message').html('To edit imported data, submit then edit.');

	console.log("TEST");	
	$.ajax({
		        type: 'POST',
		        url: "get_used_in.php",
		        data: {
		        		lemma:data.value,
		        		
		        },
		        success: function(result) {
		           console.log(result);
		           $('#parentName').html(result);
		       //    alert(result);
		        },
		        error: function(xhr,textStatus,error){
                        alert(textStatus);
		        }
		        //dataType: 'json',
		        
		    });
}








