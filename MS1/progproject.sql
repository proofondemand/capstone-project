-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2016-02-18 11:44:46
-- 服务器版本： 10.1.9-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `progproject`
--

-- --------------------------------------------------------

--
-- 表的结构 `projects`
--

CREATE TABLE `projects` (
  `p_id` int(11) NOT NULL,
  `p_title` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 转存表中的数据 `projects`
--

INSERT INTO `projects` (`p_id`, `p_title`) VALUES
(1, 'Project A'),
(2, 'Project B'),
(4, 'Project C'),
(5, 'Project D');

-- --------------------------------------------------------

--
-- 表的结构 `projectsusers`
--

CREATE TABLE `projectsusers` (
  `p_id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 转存表中的数据 `projectsusers`
--

INSERT INTO `projectsusers` (`p_id`, `u_id`) VALUES
(1, 1),
(1, 3),
(1, 6),
(2, 3),
(2, 5),
(2, 6),
(4, 1),
(5, 6);

-- --------------------------------------------------------

--
-- 表的结构 `requests`
--

CREATE TABLE `requests` (
  `r_id` int(11) NOT NULL,
  `r_project` int(11) NOT NULL,
  `r_requester` int(11) NOT NULL,
  `r_reqdate` datetime NOT NULL,
  `r_lemmatitle` varchar(10000) NOT NULL,
  `r_lemma` varchar(10000) NOT NULL,
  `r_parenttitle` varchar(100) NOT NULL,
  `r_parentname` varchar(100) NOT NULL,
  `r_status` varchar(25) NOT NULL,
  `r_contributor` int(11) DEFAULT NULL,
  `r_condate` datetime DEFAULT NULL,
  `r_comment` varchar(10000) DEFAULT NULL,
  `r_moddate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 转存表中的数据 `requests`
--

INSERT INTO `requests` (`r_id`, `r_project`, `r_requester`, `r_reqdate`, `r_lemmatitle`, `r_lemma`, `r_parenttitle`, `r_parentname`, `r_status`, `r_contributor`, `r_condate`, `r_comment`, `r_moddate`) VALUES
(1, 1, 1, '2015-03-28 00:00:00', 'msg_nStore', '	assumes h1:" forall i<n. FlexRayController (nReturn i)\r\n	recv (nC i) (nStore i) (nSend i) (nGet i)"\r\n		and h2:"DisjointSchedules n nC"\r\n		and h3:"IdenticCycleLength n nC"\r\n		and h4:"inf_disj n nSend"\r\n		and h5:"i < n"  \r\n		and h6:"forall i<n. msg (Suc 0) (nReturn i)"\r\n		and h7:"Cable n nSend recv"\r\n	shows "msg (Suc 0) (nStore i)"', '1', '1', 'IN PROGRESS', 6, '2016-02-16 12:56:00', '', '2015-03-28 00:00:00'),
(2, 1, 3, '2015-04-09 00:00:00', 'disjointFrame_Lwrong', '	assumes h1:"Â¬ DisjointSchedules n nC"  \r\n	and h2:"IdenticCycleLength n nC"\r\n	and h3:"forall i < n. FlexRayController (nReturn i) rcv \r\n		(nC i) (nStore i) (nSend i) (nGet i)"\r\n	shows "inf_disj n nSend"', '1', '1', 'IN PROGRESS', 6, '2016-02-17 19:30:39', '', '2015-04-09 00:00:00'),
(3, 1, 1, '2015-06-09 22:19:04', 'lemma_A', 'this is lemma_A', '1', '1', 'CONTRADICTION', 1, '2015-06-10 02:50:16', 'This lemma is dependent on a lemma with a contradiction', '2015-06-09 22:19:04'),
(4, 1, 1, '2015-06-09 22:19:38', 'lemma_B', 'this is lemma_B', '1', '4', 'IN PROGRESS', 1, '2015-06-10 02:50:21', '', '2015-06-09 22:19:38'),
(5, 1, 1, '2015-06-10 02:37:47', 'lemma_C', 'this is lemma_C', '1', '4', 'CONTRADICTION', 1, '2015-06-10 03:06:24', 'This lemma is dependent on a lemma with a contradiction', '2015-06-10 02:37:47'),
(6, 1, 1, '2015-06-10 03:04:22', 'lemma_D', 'this is lemma_D', '1', '6', 'CONTRADICTION', 1, '2015-06-10 05:01:39', 'This is the contradiction', '2015-06-10 03:04:22'),
(10, 5, 6, '2016-02-16 11:33:54', 'disjointFrame_L1', 'disjointFrame_L1', 'FR_proof', 'disjointFrame_L2', 'REQUESTED', NULL, NULL, NULL, '2016-02-16 11:33:54'),
(12, 5, 6, '2016-02-16 13:09:13', 'Gateway_L6', '666', 'Gateway_proof', 'GatewaySystem_L2\nGatewaySystem_L3\nGateway_L6a', 'REQUESTED', NULL, NULL, NULL, '2016-02-16 13:09:13');

-- --------------------------------------------------------

--
-- 表的结构 `users`
--

CREATE TABLE `users` (
  `u_id` int(11) NOT NULL,
  `u_email` varchar(35) NOT NULL,
  `u_pw` varchar(50) NOT NULL,
  `u_fname` varchar(25) NOT NULL,
  `u_lname` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 转存表中的数据 `users`
--

INSERT INTO `users` (`u_id`, `u_email`, `u_pw`, `u_fname`, `u_lname`) VALUES
(1, 'peter@email.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'Peter', 'Ravindra'),
(3, 'jdoe@email.com', '3da541559918a808c2402bba5012f6c60b27661c', 'John', 'Doe'),
(5, 'jsmith@email.com', 'b1b3773a05c0ed0176787a4f1574ff0075f7521e', 'John', 'Smith'),
(6, '915879198@qq.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'Callen', 'Devens');

-- --------------------------------------------------------

--
-- 表的结构 `usersrequests`
--

CREATE TABLE `usersrequests` (
  `u_id` int(11) NOT NULL,
  `r_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 转存表中的数据 `usersrequests`
--

INSERT INTO `usersrequests` (`u_id`, `r_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(3, 1),
(3, 2),
(6, 1),
(6, 2),
(6, 6);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `projectsusers`
--
ALTER TABLE `projectsusers`
  ADD PRIMARY KEY (`p_id`,`u_id`),
  ADD KEY `projectsusers_ibfk_2` (`u_id`);

--
-- Indexes for table `requests`
--
ALTER TABLE `requests`
  ADD PRIMARY KEY (`r_id`),
  ADD KEY `r_contributor` (`r_contributor`),
  ADD KEY `r_requester` (`r_requester`),
  ADD KEY `r_project` (`r_project`),
  ADD KEY `r_parentname` (`r_parentname`),
  ADD KEY `r_parenttitle` (`r_parenttitle`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`u_id`),
  ADD UNIQUE KEY `u_email` (`u_email`);

--
-- Indexes for table `usersrequests`
--
ALTER TABLE `usersrequests`
  ADD PRIMARY KEY (`u_id`,`r_id`),
  ADD KEY `usersrequests_ibfk_2` (`r_id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `projects`
--
ALTER TABLE `projects`
  MODIFY `p_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- 使用表AUTO_INCREMENT `requests`
--
ALTER TABLE `requests`
  MODIFY `r_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- 使用表AUTO_INCREMENT `users`
--
ALTER TABLE `users`
  MODIFY `u_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- 限制导出的表
--

--
-- 限制表 `projectsusers`
--
ALTER TABLE `projectsusers`
  ADD CONSTRAINT `projectsusers_ibfk_1` FOREIGN KEY (`p_id`) REFERENCES `projects` (`p_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `projectsusers_ibfk_2` FOREIGN KEY (`u_id`) REFERENCES `users` (`u_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- 限制表 `requests`
--
ALTER TABLE `requests`
  ADD CONSTRAINT `requests_ibfk_1` FOREIGN KEY (`r_requester`) REFERENCES `users` (`u_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `requests_ibfk_3` FOREIGN KEY (`r_contributor`) REFERENCES `users` (`u_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `requests_ibfk_4` FOREIGN KEY (`r_project`) REFERENCES `projects` (`p_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- 限制表 `usersrequests`
--
ALTER TABLE `usersrequests`
  ADD CONSTRAINT `usersrequests_ibfk_1` FOREIGN KEY (`u_id`) REFERENCES `users` (`u_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `usersrequests_ibfk_2` FOREIGN KEY (`r_id`) REFERENCES `requests` (`r_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
