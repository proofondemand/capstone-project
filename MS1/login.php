<?php include('topbit.inc'); ?>
<?php 
	if(!isset($_SESSION['loggedin'])){
		echo "<title>Project | Login</title>";
	}
	else{
		header("Location: index.php");
		
	}
?>
<?php include('middlebit.inc'); ?>
<?php
	include('database.php');
	
	$loginError = "";
	// php handling of requests
	if($_SERVER["REQUEST_METHOD"] == "POST") {
		// php handling of login requests
		if($_POST['action'] == "Login"){
			$email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
			$password = sha1(trim($_POST['password']));
			// find in database the email and password entered by user that is logging in
			$sql = "SELECT * FROM users WHERE u_email='$email' and u_pw='$password'";
			$count = ($result = mysqli_query($con, $sql))?mysqli_num_rows($result):0;
			// if found matching email and password, set user information as session variables
			if($count == 1){
				$_SESSION['loggedin'] = true;
				while($row = mysqli_fetch_assoc($result)){
					$_SESSION['id'] = $row["u_id"];
					$_SESSION['email'] = $row["u_email"];
					$_SESSION['firstname'] = $row["u_fname"];
					$_SESSION['lastname'] = $row["u_lname"];
				}
				echo "<p>Login successful!</p>";
				echo "<p>Welcome, ".$_SESSION['firstname']." ".$_SESSION['lastname']."!</p>";
				echo "You will be redirected to the index page. <br />";
				echo "<a href=\"index.php\">Click here if you are not redirected in 5 seconds.</a><br />";
				$secondsWait = 1; 
				echo '<meta http-equiv="refresh" content="'.$secondsWait.'">';
				
			}
			// if matching email and password not found, show relevant error message
			else{
				$loginError = "User does not exist or password is incorrect";
			}
		}
		// php handling of logout requests
		else if($_POST['action'] == "Log Out"){
			// get rid of all session variables
			session_destroy();
			echo "<p>You have been logged out successfully!</p>";
			header("Refresh: 0;");
			echo "You will be redirected to the index page. <br />";
			echo "<a href=\"index.php\">Click here if you are not redirected in 5 seconds.</a><br />";
			$secondsWait = 1; 
			echo '<meta http-equiv="refresh" content="'.$secondsWait.'">';
			
		}
		else{
			header("Location: login.php");
		}
	}
	if(!isset($_SESSION['loggedin'])){
	?>	<p><h1 class="contentheading">Log In</h1></p>
		<span class="error"><?php echo $loginError;?></span>
		<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
			<p>Email:<br/>
			<input type="text" name="email" size="20"/><br/></p>
			<p>Password:<br/>
			<input type="password" name="password"/><br/></p>
			<p><input type="submit" name="action" value="Login"/><br/></p>
		</form>
	<?php
	}
	// show page as member page instead if already logged in
	else if(isset($_SESSION['loggedin']) && $_SERVER["REQUEST_METHOD"] != "POST"){
	?>	<p><h1 class="contentheading">Member Page</h1></p>
		<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
			<p><em>Full Name:</em> <?php echo trim($_SESSION['firstname'])." ".trim($_SESSION['lastname']);?><br/>
			<a href="subscriptions.php?type=request"><em>Number of Requests:</em> <?php
				$userid = $_SESSION['id'];
				$reqsql = "SELECT r_id FROM requests WHERE r_requester = '$userid'";
				$count = ($reqresult = mysqli_query($con, $reqsql))?mysqli_num_rows($reqresult):0;
				echo $count;
			?></a><br/>
			<a href="subscriptions.php?type=solution"><em>Number of Solutions:</em> <?php
				$userid = $_SESSION['id'];
				$reqsql = "SELECT r_id FROM requests WHERE r_contributor = '$userid'";
				$count = ($reqresult = mysqli_query($con, $reqsql))?mysqli_num_rows($reqresult):0;
				echo $count;
			?></a><br/>
			<br/>			<a href="subscriptions.php?type=subcribe"><em>Subcriptions</em></a>
			<br />
			<br/>
<input type="submit" name="action" value="Log Out"/>
		</form>
	<?php
	}
?>
<?php include('endbit.inc'); ?>