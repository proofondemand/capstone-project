<?php
include ('topbit.inc');
?>
<title>Project | Projects</title>
<?php
	include ('middlebit.inc');
 ?>
<?php
include('database.php');
$user_id = $_SESSION['id'];
$type = $_GET['type'];
$sort = $_GET['sort'];

$colors = array(
"CONTRADICTION" => "red",
"SOLVED" => "limegreen"
);
// if the user has subscribed to request(s)
if($type == "request"){
$reqsql = "SELECT r.r_id, r.r_project, r.r_requester, u1.u_fname as r_reqfname, u1.u_lname as r_reqlname, r.r_reqdate,
r.r_lemmatitle, r.r_lemma, r.r_parenttitle, r.r_parentname, r.r_status, r.r_contributor, u2.u_fname as r_confname,
u2.u_lname as r_conlname, r.r_condate, r.r_comment, r.r_moddate 
FROM requests as r LEFT OUTER JOIN users as u1 ON r.r_requester = u1.u_id
LEFT OUTER JOIN users as u2 ON r.r_contributor = u2.u_id
INNER JOIN projects as pr ON r.r_project = pr.p_id
WHERE r.r_requester = $user_id";
}
else if($type=='solution'){
		$reqsql = "SELECT r.r_id, r.r_project, r.r_requester, u1.u_fname as r_reqfname, u1.u_lname as r_reqlname, r.r_reqdate, 
			r.r_lemmatitle, r.r_lemma, r.r_parenttitle, r.r_parentname, r.r_status, r.r_contributor, u2.u_fname as r_confname, 
			u2.u_lname as r_conlname, r.r_condate, r.r_comment, r.r_moddate 
			FROM requests as r LEFT OUTER JOIN users as u1 ON r.r_requester = u1.u_id 
			LEFT OUTER JOIN users as u2 ON r.r_contributor = u2.u_id 
			INNER JOIN projects as pr ON r.r_project = pr.p_id
			WHERE r.r_contributor = $user_id and r.r_status = 'SOLVED'";
}
elseif ($type == "1") {
$reqsql = "SELECT r.r_id, r.r_project, r.r_requester, u1.u_fname as r_reqfname, u1.u_lname as r_reqlname, r.r_reqdate,
r.r_lemmatitle, r.r_lemma, r.r_parenttitle, r.r_parentname, r.r_status, r.r_contributor, u2.u_fname as r_confname,
u2.u_lname as r_conlname, r.r_condate, r.r_comment, r.r_moddate 
FROM requests as r LEFT OUTER JOIN users as u1 ON r.r_requester = u1.u_id
LEFT OUTER JOIN users as u2 ON r.r_contributor = u2.u_id
INNER JOIN projects as pr ON r.r_project = pr.p_id
WHERE r.r_status = 'IN PROGRESS'";

}
elseif ($type == "2") {
$reqsql = "SELECT r.r_id, r.r_project, r.r_requester, u1.u_fname as r_reqfname, u1.u_lname as r_reqlname, r.r_reqdate,
r.r_lemmatitle, r.r_lemma, r.r_parenttitle, r.r_parentname, r.r_status, r.r_contributor, u2.u_fname as r_confname,
u2.u_lname as r_conlname, r.r_condate, r.r_comment, r.r_moddate
FROM requests as r LEFT OUTER JOIN users as u1 ON r.r_requester = u1.u_id
LEFT OUTER JOIN users as u2 ON r.r_contributor = u2.u_id
INNER JOIN projects as pr ON r.r_project = pr.p_id
WHERE r.r_status = 'SOLVED'";

}
elseif ($type == "3") {
$reqsql = "SELECT r.r_id, r.r_project, r.r_requester, u1.u_fname as r_reqfname, u1.u_lname as r_reqlname, r.r_reqdate,
r.r_lemmatitle, r.r_lemma, r.r_parenttitle, r.r_parentname, r.r_status, r.r_contributor, u2.u_fname as r_confname,
u2.u_lname as r_conlname, r.r_condate, r.r_comment, r.r_moddate
FROM requests as r LEFT OUTER JOIN users as u1 ON r.r_requester = u1.u_id
LEFT OUTER JOIN users as u2 ON r.r_contributor = u2.u_id
INNER JOIN projects as pr ON r.r_project = pr.p_id
WHERE r.r_status = 'CONTRADICTION'";
}
elseif ($type == "4") {
$reqsql = "SELECT r.r_id, r.r_project, r.r_requester, u1.u_fname as r_reqfname, u1.u_lname as r_reqlname, r.r_reqdate,
r.r_lemmatitle, r.r_lemma, r.r_parenttitle, r.r_parentname, r.r_status, r.r_contributor, u2.u_fname as r_confname,
u2.u_lname as r_conlname, r.r_condate, r.r_comment, r.r_moddate
FROM requests as r LEFT OUTER JOIN users as u1 ON r.r_requester = u1.u_id
LEFT OUTER JOIN users as u2 ON r.r_contributor = u2.u_id
WHERE r.r_status = 'REQUESTED'";
}
else{
$reqsql = "SELECT r.r_id, r.r_project, r.r_requester, u1.u_fname as r_reqfname, u1.u_lname as r_reqlname, r.r_reqdate, r.r_lemma, r.r_parenttitle,
r.r_parentname, r.r_status, r.r_contributor, u2.u_fname as r_confname, u2.u_lname as r_conlname, r.r_condate, r.r_comment, r.r_moddate
FROM requests as r LEFT OUTER JOIN users as u1 ON r.r_requester = u1.u_id
LEFT OUTER JOIN users as u2 ON r.r_contributor = u2.u_id
INNER JOIN projects as pr ON r.r_project = pr.p_id";
}

if ($sort == "a") {
	$reqsql .= ' ORDER BY r.r_reqdate;';
	
}
elseif($sort == "d"){
	$reqsql .= ' ORDER BY r.r_reqdate DESC;';
}


$reqcount = ($reqresult = mysqli_query($con, $reqsql))?mysqli_num_rows($reqresult):0;

if($reqcount > 0){
?>
<h2>You have subscribed to the following requests: </h2> <br /> 
<form>
	Sort By: <select name="users" onchange="showUser(this.value)">
		<option value="">Select:</option>
		<option value="1">In Progress</option>
		<option value="2">Solved</option>
		<option value="3">Contradiction</option>
		<option value="4">Reuqested</option>
		<option value="5">All</option>
	</select>
	<button onclick="myFunction()" class="dropbtn" type="button">Order By</button>
</form>  
  <div id="myDropdown" class="dropdown-content">
    <a href="subscriptions.php?type=<?php echo $type;?>&sort=a">Oldest-Newest</a>
     <a href="subscriptions.php?type=<?php echo $type;?>&sort=d">Newest-Oldest</a>
    <a href="#">Another Sorting Method</a>
 </div>


<script type="text/javascript">
	// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
</script>

<div class="requests">
<table style="width:100%">
<tr>
<th>Request from</th>
<th>Lemma</th>
<th>Used in</th>
<th>Status</th>
</tr>
<?php
while($reqrow = mysqli_fetch_assoc($reqresult)){
?>
<tr style="background-color: <?php echo(isset($colors[$reqrow["r_status"]]) ? $colors[$reqrow["r_status"]] : "white"); ?>">
<td><?php echo $reqrow["r_reqfname"] . " " . $reqrow["r_reqlname"]; ?><br/><?php echo formatDate($reqrow["r_reqdate"]); ?></td>
<td><pre><?php echo htmlspecialchars($reqrow["r_lemma"]); ?></pre><br/><br/>Last modified date: <em><?php echo formatDate($reqrow["r_moddate"]); ?></em></td>
<td><pre><?php echo $reqrow["r_parenttitle"]; ?>:<br/><?php echo $reqrow["r_parentname"]; ?></pre></td>
<td><?php echo $reqrow["r_status"]; ?><br/><?php echo($reqrow["r_confname"] === NULL && $reqrow["r_conlname"] === NULL) ? ('') : ($reqrow['r_confname'] . ' ' . $reqrow['r_conlname'] . '<br/>' . formatDate($reqrow['r_condate'])); ?><br/><br/>
<pre><?php echo($reqrow['r_comment'] === NULL) ? ('') : ($reqrow["r_comment"]); ?></pre>
<?php if($_SESSION['id'] == $reqrow["r_requester"]){?> 
<input type="button" value="Delete Request" onClick='deleteRequest(<?php echo $reqrow["r_project"]. ",".$reqrow["r_id"];?>)'/>
<input type="checkbox" name="confirm" id="confirm" checked  onClick="confirm('you will no longer recieve any notifications from this request')" value="click">Email notification </input>
<?php }?>

</tr>
<?php
}
?>
</table>
<input type="button" value="Back" OnClick="window.location.href='index.php'">  </input>
<?php

}
else {
	?> <h2>No Requests: 
		<input type="button" value="Back" OnClick="window.location.href='index.php'">  </input></h2> 
		<?php
}
include('endbit.inc');
?>